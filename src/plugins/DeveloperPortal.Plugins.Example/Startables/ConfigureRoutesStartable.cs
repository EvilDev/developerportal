﻿using System.Web.Mvc;
using System.Web.Routing;
using Castle.Core;

namespace DeveloperPortal.Plugins.Example.Startables
{
    public class ConfigureRoutesStartable : IStartable
    {
        private readonly RouteCollection _routes;

        public ConfigureRoutesStartable(RouteCollection routes)
        {
            _routes = routes;
        }

        public void Start()
        {
            _routes.MapRoute(
                name: "Example",
                url: "Example/{controller}/{action}/{id}");
        }

        public void Stop()
        {
        }
    }
}