﻿using System;
using DeveloperPortal.Infrastructure;

namespace DeveloperPortal.Plugins.Example
{
    public class ExampleMetatadata : IPluginMetadata
    {
        public string Name { get; private set; }
        public Version Version { get; private set; }
        public string Author { get; private set; }
        public DateTime ReleaseDate { get; private set; }

        public ExampleMetatadata()
        {
            Name = "Example Plugin for Developer Portal";
	    Version = new Version("1.0.0");
            Author = "Evil Dev";
            ReleaseDate = DateTime.Now;
        }
    }
}