﻿using System;

namespace DeveloperPortal.Infrastructure
{
    public interface IPluginMetadata
    {
        string Name { get; }

        Version Version { get; }

        string Author { get; }

        DateTime ReleaseDate { get; }
    }
}