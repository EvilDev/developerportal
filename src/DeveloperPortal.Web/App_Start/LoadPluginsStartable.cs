﻿using System.IO;
using System.Linq;
using System.Web;
using Castle.Core;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Castle.Windsor.Installer;
using DeveloperPortal.Infrastructure;
using EvilDev.Commons.Extensions;

namespace DeveloperPortal.Web
{
    public class LoadPluginsStartable : IStartable
    {
        private readonly IWindsorContainer _container;

        public LoadPluginsStartable(IWindsorContainer container)
        {
            _container = container;
        }

        public void Start()
        {
            var pluginDir = Path.Combine(HttpRuntime.AppDomainAppPath, "Plugins");
	    
            Directory.GetDirectories(pluginDir,"*", SearchOption.AllDirectories)
				   .Select(dir => new AssemblyFilter(dir))
				   .ForEach(filter =>
				       {
				           _container.Install(FromAssembly.InDirectory(filter));
				           _container.Register(Classes.FromAssemblyInDirectory(filter)
				                                      .BasedOn<IPluginMetadata>().WithServiceBase());
				       });
        }

        public void Stop()
        {
        }
    }
}