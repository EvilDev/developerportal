﻿using System.Web.Mvc;
using Castle.Core;

namespace DeveloperPortal.Web
{
    public class ConfigureAreasStartable : IStartable
    {
        public void Start()
        {
	    AreaRegistration.RegisterAllAreas();
        }

        public void Stop()
        {
        }
    }
}