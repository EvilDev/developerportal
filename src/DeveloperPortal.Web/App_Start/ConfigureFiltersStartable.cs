﻿using System.Web.Mvc;
using Castle.Core;

namespace DeveloperPortal.Web
{
    public class ConfigureFiltersStartable : IStartable
    {
        private readonly GlobalFilterCollection _filters;

        public ConfigureFiltersStartable(GlobalFilterCollection filters)
        {
            _filters = filters;
        }

        public void Start()
        {
            _filters.Add(new HandleErrorAttribute());
        }

        public void Stop()
        {
        }
    }
}