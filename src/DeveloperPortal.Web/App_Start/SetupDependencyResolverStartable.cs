﻿using System.Web.Mvc;
using Castle.Core;

namespace DeveloperPortal.Web
{
    public class SetupDependencyResolverStartable : IStartable
    {
            private readonly IDependencyResolver _resolver;

            public SetupDependencyResolverStartable(IDependencyResolver resolver)
            {
                _resolver = resolver;
            }

            public void Start()
            {
                DependencyResolver.SetResolver(_resolver);
            }

            public void Stop()
            {
            }
   }
}