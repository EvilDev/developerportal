﻿using System.Collections.Generic;
using System.Web.Mvc;
using DeveloperPortal.Infrastructure;

namespace DeveloperPortal.Web.Controllers
{
    // [Authorize]
    public class PluginsController : Controller
    {
        private readonly IEnumerable<IPluginMetadata> _plugins;

        public PluginsController(IEnumerable<IPluginMetadata> plugins)
        {
            _plugins = plugins;
        }

	public ActionResult Index()
	{
	    return View(_plugins);
	}
    }
}