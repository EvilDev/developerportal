﻿using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Castle.Core;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Evildev.Commons.Windsor.Mvc;

namespace DeveloperPortal.Web.Installers
{
    public class StartablesInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Classes.FromThisAssembly()
                                      .BasedOn<IStartable>()
                                      .WithServiceBase());

            container.Register(Component.For<IWindsorContainer>().Instance(container));

            container.Register(Component.For<HttpConfiguration>().Instance(GlobalConfiguration.Configuration));
            container.Register(Component.For<BundleCollection>().Instance(BundleTable.Bundles));
            container.Register(Component.For<RouteCollection>().Instance(RouteTable.Routes));
            container.Register(Component.For<GlobalFilterCollection>().Instance(GlobalFilters.Filters));
            container.Register(Component.For<IDependencyResolver>().ImplementedBy<WindsorDependencyResolver>());
        }
    }
}