﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using DeveloperPortal.Infrastructure;

namespace DeveloperPortal.Web.Installers
{
    public class PluginInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Classes.FromAssemblyInDirectory(new AssemblyFilter("Plugins"))
					.BasedOn<IPluginMetadata>()
					.WithServiceBase());
        }
    }
}