﻿using System.Web.Http.Dependencies;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Evildev.Commons.Windsor.Mvc;
using WindsorDependencyResolver = Evildev.Commons.Windsor.Mvc.WebApi.WindsorDependencyResolver;

namespace DeveloperPortal.Web.Installers
{
    public class ApiInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Component.For<IDependencyResolver>().ImplementedBy<WindsorDependencyResolver>());
            container.Register(Component.For<IDependencyScope>().ImplementedBy<WindsorDependencyScope>().LifestyleTransient());
        }
    }
}