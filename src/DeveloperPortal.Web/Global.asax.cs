﻿using System.Web;
using Castle.Facilities.Startable;
using Castle.Facilities.TypedFactory;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.Resolvers.SpecializedResolvers;
using Castle.Windsor;
using Castle.Windsor.Installer;

namespace DeveloperPortal.Web
{
    public class DeveloperPortal : HttpApplication
    {
        private IWindsorContainer _container;

        public DeveloperPortal(){} // Empty default constructor for framework



        protected void Application_Start()
        {
            _container = new WindsorContainer();
            _container.AddFacility<StartableFacility>();
            _container.AddFacility<TypedFactoryFacility>();
            _container.Register(Component.For<HttpApplication>().Instance(this));
            _container.Kernel.Resolver.AddSubResolver(new CollectionResolver(_container.Kernel, false));

            _container.Install(FromAssembly.InThisApplication());
        }

	protected void Application_Error()
	{
	    
	}

	protected void Application_End()
	{
	    _container.Dispose();
	}
    }
}